amqp==2.6.1
argon2-cffi==20.1.0
asgiref==3.3.1
beautifulsoup4==4.9.3
billiard==3.6.3.0
celery==4.3.0
certifi==2020.6.20
cffi==1.14.4
chardet==3.0.4
coverage==5.3
cryptography==3.1.1
dealer==2.1.0
Django==3.1.5
django-autocomplete-light==3.8.1
django-bootstrap-swt==2.0.1
django-bootstrap4==2.2.0


# DJANGO
Django~=3.1.2
django-extensions==3.0.9
django-simple-captcha==0.5.12
django-celery-beat==2.0.0
django-debug-toolbar==3.1.1
django-extensions==3.0.9
django-filter==2.4.0
django-fontawesome-5==1.0.18
django-formtools==2.2
django-leaflet==0.27.1
django-mathfilters==1.0.0
django-nose==1.4.7
django-query-parameters==0.2.3
django-ranged-response==0.2.0
django-redis==4.12.1
django-simple-captcha==0.5.12
django-tables2==2.3.1
django-timezone-field==4.1.1
djangorestframework==3.12.1
django-debug-toolbar==3.1.1
django-formtools~=2.2
django-mptt~=0.11.0

# OTHER
certifi==2020.6.20
idna==2.10
importlib-metadata==1.7.0
kombu==4.6.11
lxml==4.5.2
model-bakery==1.2.0
nose==1.3.7
Pillow==7.2.0
psycopg2==2.8.6
pycparser==2.20
pycurl==7.43.0.6
python-crontab==2.5.1
python-dateutil==2.8.1
pytz==2020.5
redis==3.5.3
requests==2.24.0
six==1.15.0
soupsieve==2.1
sqlparse==0.4.1
tablib==3.0.0
urllib3==1.25.10
argon2-cffi==20.1.0
celery==4.3.1
redis==3.5.3
Pillow==7.2.0
cryptography==3.1.1
coverage==5.3
model-bakery==1.2.0
python-dateutil==2.8.1
xmltodict==0.12.0
Werkzeug==1.0.1

# For git revision rendering
dealer==2.1.0


# UNUSED PACKAGES
## Keep them until we can be sure they can be removed without problems
#chardet==3.0.4
#markdown
#pysxm
#six==1.12.0
=======
vine==1.3.0
xmltodict==0.12.0
zipp==3.4.0
